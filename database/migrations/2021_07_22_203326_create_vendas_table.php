<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateVendasTable.
 */
class CreateVendasTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('vendas', function(Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_vendedor');
            $table->foreign('id_vendedor')
                ->references('id')
                ->on('vendedors')
                ->onDelete('cascade');
            $table->decimal('valor_venda',9,3);
            $table->decimal('valor_comissao',9,3);
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('vendas');
	}
}
