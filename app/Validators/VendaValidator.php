<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class VendaValidator.
 *
 * @package namespace App\Validators;
 */
class VendaValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id_vendedor' => 'required|int',
            'valor_venda' => 'required',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'id_vendedor' => 'required|int',
            'valor_venda' => 'required',
        ],
    ];
}
