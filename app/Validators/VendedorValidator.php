<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class VendedorValidator.
 *
 * @package namespace App\Validators;
 */
class VendedorValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'nome' => 'required|string|max:255',
            'email' => 'required|email|unique:vendedors,email',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'nome' => 'required|string|max:255',
            'email' => 'required',
        ],
    ];



    protected $messages = [
        'required' => 'O campo :attribute é obrigatório.',
    ];
}
