<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Venda.
 *
 * @package namespace App\Entities;
 */
class Venda extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['valor_venda','valor_comissao','id_vendedor'];

    /**
     * @var string[]
     */
    protected $hidden = ['updated_at','id_vendedor'];

    protected $casts = [
        'created_at'  => 'date:d/m/Y h:i:s',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function vendedor()
    {
        return $this->hasOne(Vendedor::class, 'id', 'id_vendedor');
    }
}
