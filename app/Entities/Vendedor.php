<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Vendedor.
 *
 * @package namespace App\Entities;
 */
class Vendedor extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nome','email'];


    /**
     * @var string[]
     */
    protected $hidden = ['created_at','updated_at'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function vendas()
    {
        return $this->hasMany(Venda::class, 'id_vendedor');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function valorComissao()
    {
        return $this->hasMany(Venda::class, 'id_vendedor');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function valorVendas()
    {
        return $this->hasMany(Venda::class, 'id_vendedor');
    }
}
