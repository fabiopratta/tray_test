<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EnviaVendasDiario extends Mailable
{
    use Queueable, SerializesModels;


    public $vendas;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($vendas)
    {
        $this->vendas = $vendas;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Relatório de vendas diário')
            ->view('emails.vendas_diario')
            ->from('fabiobrotas@hotmail.com');
    }
}
