<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\VendedorRepository;
use App\Entities\Vendedor;
use App\Validators\VendedorValidator;

/**
 * Class VendedorRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class VendedorRepositoryEloquent extends BaseRepository implements VendedorRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Vendedor::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return VendedorValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
