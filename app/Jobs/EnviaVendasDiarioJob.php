<?php

namespace App\Jobs;

use App\Mail\EnviaVendasDiario;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class EnviaVendasDiarioJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $vendasDia;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($vendasDia)
    {
        $this->vendasDia = $vendasDia;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to('fabiobrotas@hotmail.com')->send(new EnviaVendasDiario($this->vendasDia));
    }
}
