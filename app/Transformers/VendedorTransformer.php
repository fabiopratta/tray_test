<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Vendedor;

/**
 * Class VendedorTransformer.
 *
 * @package namespace App\Transformers;
 */
class VendedorTransformer extends TransformerAbstract
{
    /**
     * Transform the Vendedor entity.
     *
     * @param \App\Entities\Vendedor $model
     *
     * @return array
     */
    public function transform(Vendedor $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
