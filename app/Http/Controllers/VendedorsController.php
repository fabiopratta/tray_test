<?php

namespace App\Http\Controllers;

use App\Services\VendedorService;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\VendedorCreateRequest;
use App\Http\Requests\VendedorUpdateRequest;
use App\Validators\VendedorValidator;

/**
 * Class VendedorsController.
 *
 * @package namespace App\Http\Controllers;
 */
class VendedorsController extends Controller
{

    /**
     * @var VendedorService
     */
    protected $service;

    /**
     * @var VendedorValidator
     */
    protected $validator;

    /**
     * VendedorsController constructor.
     * @param VendedorService $service
     * @param VendedorValidator $validator
     */
    public function __construct(VendedorService $service, VendedorValidator $validator)
    {
        $this->service = $service;
        $this->validator = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->wantsJson()) {
            return response()->json($this->service->listarTodos());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  VendedorCreateRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(VendedorCreateRequest $request)
    {
        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
            if ($request->wantsJson()) {
                return response()->json(
                    $this->service->inserir([
                            'nome' => $request->input('nome'),
                            'email' => $request->input('email')
                    ])
                );
            }
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (request()->wantsJson()) {
            return response()->json($this->service->pegarUm($id));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  VendedorUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(VendedorUpdateRequest $request, $id)
    {
        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
            if ($request->wantsJson()) {
                return response()->json(
                    $this->service->atualizar([
                        'nome' => $request->input('nome'),
                        'email' => $request->input('email')
                    ], $id)
                );
            }
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (request()->wantsJson()) {
            return response()->json($this->service->apagar($id));
        }
    }
}
