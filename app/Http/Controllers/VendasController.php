<?php

namespace App\Http\Controllers;

use App\Services\VendaService;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\VendaCreateRequest;
use App\Http\Requests\VendaUpdateRequest;
use App\Repositories\VendaRepository;
use App\Validators\VendaValidator;

/**
 * Class VendasController.
 *
 * @package namespace App\Http\Controllers;
 */
class VendasController extends Controller
{

    /**
     * @var VendaValidator
     */
    protected $validator;

    /**
     * @var VendaService
     */
    protected $service;

    /**
     * VendasController constructor.
     *
     * @param VendaRepository $repository
     * @param VendaValidator $validator
     */
    public function __construct(VendaService $service, VendaValidator $validator)
    {
        $this->validator  = $validator;
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->wantsJson()) {
            return response()->json($this->service->listarTodos());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  VendaCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(VendaCreateRequest $request)
    {
        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            if ($request->wantsJson()) {
                return response()->json(
                    $this->service->inserir([
                        'valor_venda' => $request->input('valor_venda'),
                        'id_vendedor' => $request->input('id_vendedor'),
                    ])
                );
            }
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (request()->wantsJson()) {
            return response()->json($this->service->pegarUm($id));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  VendaUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(VendaUpdateRequest $request, $id)
    {
        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            if ($request->wantsJson()) {
                return response()->json(
                    $this->service->atualizar([
                        'valor_venda' => $request->input('valor_venda'),
                        'id_vendedor' => $request->input('id_vendedor'),
                    ], $id)
                );
            }
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (request()->wantsJson()) {
            return response()->json($this->service->apagar($id));
        }
    }
}
