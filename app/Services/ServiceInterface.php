<?php

namespace App\Services;

interface ServiceInterface
{
    public function listarTodos();
    public function inserir(array $insert);
    public function pegarUm($id);
    public function atualizar(array $update, $id);
    public function apagar($id);
}
