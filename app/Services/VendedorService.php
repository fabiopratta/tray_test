<?php

namespace App\Services;

use App\Repositories\VendedorRepository;
use Illuminate\Support\Facades\DB;

class VendedorService implements ServiceInterface
{
    /**
     * @var VendedorRepository
     */
    protected $repository;

    /**
     * VendedorsController constructor.
     *
     * @param VendedorRepository $repository
     * @param VendedorValidator $validator
     */
    public function __construct(VendedorRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * get all vendedors
     * @return mixed
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function listarTodos()
    {
        return $this->repository
            ->withCount(['valorComissao as total_comissao' => function ($query) {
                $query->select(DB::raw('SUM(valor_comissao)'));
            }])->withCount(['valorVendas as total_vendas' => function ($query) {
                $query->select(DB::raw('SUM(valor_venda)'));
            }])
            ->all();
    }

    /**
     * @param array $insert
     * @return mixed
     */
    public function inserir(array $insert)
    {
        if (empty($insert)) {
            return ['error' => true,'message' => 'Array passado está vazio.'];
        }

        return $this->repository->create($insert);
    }

    /**
     * find one vendedors
     * @param $id
     * @return mixed
     */
    public function pegarUm($id)
    {
        if (empty($id)) {
            return ['error' => true,'message' => 'ID é obrigatório.'];
        }

        return $this->repository->with(['vendas'])
            ->withCount(['valorComissao as total_comissao' => function ($query) {
                $query->select(DB::raw('SUM(valor_comissao)'));
            }])->withCount(['valorVendas as total_vendas' => function ($query) {
                $query->select(DB::raw('SUM(valor_venda)'));
            }])
            ->find($id);
    }

    /**
     * @param array $update
     * @param $id
     */
    public function atualizar(array $update, $id)
    {
        if (empty($id)) {
            return ['error' => true,'message' => 'ID é obrigatório.'];
        }

        if (empty($update)) {
            return ['error' => true,'message' => 'Array passado está vazio.'];
        }

        return $this->repository->update($update, $id);
    }

    /**
     * @param $id
     * @return int
     */
    public function apagar($id)
    {
        if (empty($id)) {
            return ['error' => true,'message' => 'ID é obrigatório.'];
        }

        return $this->repository->delete($id);
    }
}
