<?php

namespace App\Services;

use App\Repositories\VendaRepository;
use App\Repositories\VendedorRepository;

class VendaService implements ServiceInterface
{

    /**
     * @var VendaRepository
     */
    protected $repository;


    protected $vendedorRepository;

    /**
     * VendasController constructor.
     *
     * @param VendaRepository $repository
     */
    public function __construct(VendaRepository $repository, VendedorRepository $vendedorRepository)
    {
        $this->repository = $repository;
        $this->vendedorRepository = $vendedorRepository;
    }

    /**
     * @return mixed
     */
    public function listarTodos()
    {
        return $this->repository->with(['vendedor'])->all();
    }

    /**
     * @param array $insert
     */
    public function inserir(array $insert)
    {
        if (empty($insert)) {
            return ['error' => true,'message' => 'Array passado está vazio.'];
        }

        $comissao = ($insert['valor_venda'] / 100) * 8.5;
        $insert['valor_comissao'] = round($comissao, 2);

        $venda = $this->repository->create($insert);

        if ($venda) {
            $vendedor = $this->vendedorRepository->find($insert['id_vendedor']);

            $arrResult = [
                'id' => $venda->id,
                'nome' => $vendedor->nome,
                'email' => $vendedor->email,
                'comissao' => $insert['valor_comissao'],
                'valor_venda' => $venda->valor_venda,
                'data_venda' => $venda->created_at->format('d/m/Y H:i:s')
            ];

            return $arrResult;
        } else {
            return ['error' => true,'message' => 'Erro ao salvar.'];
        }
    }

    /**
     * @param $id
     * @return mixed
     */
    public function pegarUm($id)
    {
        if (empty($id)) {
            return ['error' => true,'message' => 'ID é obrigatório.'];
        }

        return $this->repository->find($id);
    }

    /**
     * @param array $update
     * @param $id
     * @return mixed
     */
    public function atualizar(array $update, $id)
    {
        if (empty($id)) {
            return ['error' => true,'message' => 'ID é obrigatório.'];
        }

        if (empty($update)) {
            return ['error' => true,'message' => 'Array passado está vazio.'];
        }

        return $this->repository->update($update, $id);
    }

    /**
     * @param $id
     * @return int
     */
    public function apagar($id)
    {
        if (empty($id)) {
            return ['error' => true,'message' => 'ID é obrigatório.'];
        }

        return $this->repository->delete($id);
    }
}
