<?php


use Illuminate\Support\Facades\Route;
use App\Http\Controllers\{VendedorsController, VendasController};

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'api'], function(){
    Route::apiResource('vendedor', VendedorsController::class);
    Route::apiResource('venda', VendasController::class);
});
