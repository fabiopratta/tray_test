<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Oxygen:wght@300;400;700&family=Raleway:ital,wght@0,100;1,400&display=swap" rel="stylesheet">
    <title>Relatório de vendas diário</title>
</head>
<body>
<div class="container">
    <img class="mb-4" src="https://raw.githubusercontent.com/fabiopratta/tray_test/main/public/assets/img/logo.jpg" alt="Tray E-commerce" width="200">
    <h1 class="h3 mb-3 font-weight-normal" style="font-family: 'Oxygen', sans-serif;">Relatório de vendas diário</h1>
    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Vendedor</th>
            <th scope="col">Valor venda</th>
            <th scope="col">Comissão</th>
        </tr>
        </thead>
        <tbody>
        @php
            $valorTotalVenda = 0;
            $valorTotalComissao = 0;
        @endphp
        @foreach($vendas as $venda)
            <tr>
                <th scope="row">{{$venda->id}}</th>
                <td>{{$venda->vendedor->nome}}</td>
                <td>R$ {{number_format($venda->valor_venda, 2) }}</td>
                <td>R$ {{number_format($venda->valor_comissao, 2) }}</td>
            </tr>

            @php
                $valorTotalVenda = $valorTotalVenda + $venda->valor_venda;
                $valorTotalComissao = $valorTotalComissao + $venda->valor_comissao;
            @endphp

        @endforeach
        </tbody>
        <tfoot>
        <tr>
            <th scope="col"></th>
            <th scope="col"></th>
            <th scope="col">R$ {{number_format($valorTotalVenda,2)}}</th>
            <th scope="col">R$ {{number_format($valorTotalComissao,2)}}</th>
        </tr>
        </tfoot>
    </table>
</div>
</body>
</html>
