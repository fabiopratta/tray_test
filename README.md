
## Sistema de teste para Tray

Backend desenvolvido em PHP utilizando Laravel, recomendações:
 - Para utilizar o Schedule diário de envio de email <code>php artisan schedule:work</code>.
 - Envio também uma Collection do Postman <a href="https://github.com/fabiopratta/tray_test/blob/main/Test_Tray.postman_collection.json">https://github.com/fabiopratta/tray_test/blob/main/Test_Tray.postman_collection.json para testes.
 - Alterar as configurações em seu arquivo <code>.env</code>, para banco de dados e envio de email.

<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary>Endpoints</summary>
  <ol>
    <li>
      <a href="#Criar Vendedor">Criar vendedor</a>
    </li>
    <li>
      <a href="#Listar todos os Vendedores">Listar todos os Vendedores</a>
    </li>
    <li>
      <a href="#Lançar nova Venda">Lançar nova Venda</a>
    </li>
    <li>
      <a href="#Listar todas as vendas de um vendedor">Listar todas as vendas de um vendedor</a>
    </li>
  </ol>
</details>

## Criar Vendedor
<img id="Criar Vendedor" src="https://img.shields.io/static/v1?label=&message=POST&color=blue"> <code> {{URL}}/api/vendedor </code>
<br/>Retorna os dados do vendedor inserido

### Parametros
<table>
    <tr>
        <td>nome <code>STRING</code></td>
        <td>obrigatório</td>
    </tr>
    <tr>
        <td>email <code>STRING</code></td>
        <td>obrigatório</td>
    </tr>
</table>

### Resposta
```json
{
    "nome": "Vendedor",
    "email": "vendedor@teste.com.br",
    "id": 1
}
```


## Listar todos os Vendedores
<img id="Listar todos os Vendedores" src="https://img.shields.io/static/v1?label=&message=GET&color=green"> <code> {{URL}}/api/vendedor </code>
<br/>Retorna todos os vendedores

### Resposta
```json
[
    {
        "id": 1,
        "nome": "Vendedor",
        "email": "vendedor@teste.com.br",
        "total_comissao": "27.850",
        "total_vendas": "280.000"
    },.....
]
```

## Lançar nova Venda
<img id="Lançar nova Venda" src="https://img.shields.io/static/v1?label=&message=POST&color=blue"> <code> {{URL}}/api/venda </code>
<br/>Retorna os dados da venda inserida

### Parametros
<table>
    <tr>
        <td>id_vendedor <code>INT</code></td>
        <td>obrigatório</td>
    </tr>
    <tr>
        <td>valor_venda <code>DECIMAL</code></td>
        <td>obrigatório</td>
    </tr>
</table>

### Resposta
```json
{
    "id": 1,
    "nome": "Vendedor",
    "email": "vendedor@teste.com.br",
    "comissao": 0.85,
    "valor_venda": "10.00",
    "data_venda": "23/07/2021 11:03:35"
}
```

## Listar todas as vendas de um vendedor
<img id="Listar todas as vendas de um vendedor" src="https://img.shields.io/static/v1?label=&message=GET&color=green"> <code> {{URL}}/api/vendedor/{ID_VENDEDOR} </code>
<br/>Retorna todas as vendas do vendedor

### Resposta
```json
{
    "id": 1,
    "nome": "Vendedor",
    "email": "vendedor@teste.com.br",
    "total_comissao": "27.850",
    "total_vendas": "280.000",
    "vendas": [
        {
            "id": 1,
            "valor_venda": "180.000",
            "valor_comissao": "18.000",
            "created_at": "22/07/2021 09:54:40"
        },
        {
            "id": 2,
            "valor_venda": "100.000",
            "valor_comissao": "10.000",
            "created_at": "22/07/2021 09:54:40"
        },.....
    ]
}
```


